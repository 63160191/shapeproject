/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Square {
    private double S;
    public Square(double S){
        this.S = S;
    }
    public double calArea(){
        double result = S*S;
        return result;
    }
    public double getS(){
        return this.S;
    }
    public void setS(double S){
        if(S<=0){
            System.err.println("Error : Area must more than zero!!!");
            return;
        }
        this.S = S;
    }
    
    @Override
    public String toString(){
        return "Area of square1(S = " + getS() + ") is " + calArea();
    }
    
}
