/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Triangle {
    private double b;
    private double h;
    public static final double x=0.5;
    public Triangle(double b,double h){
        this.b=b;
        this.h=h;
    }public double calTriangle(){
        return x*b*h;
    }public void setb(double b){
        if(b<=0){
            System.err.println("Error : Area must more than zero!!!");
            return;
        }
        this.b=b;
    }public void seth(double h){
        if(h<=0){
            System.err.println("Error : Area must more than zero!!!");
            return;
        }
        this.h=h;
    }public void setboth(double b,double h){
        if(b<=0||h<=0){
            System.err.println("Error : Area must more than zero!!!");
            return;
        }
        this.b=b;
        this.h=h;
    }public double getb(){
        return b;
    }public double geth(){
        return h;
    }
    @Override
    public String toString(){
        return "Area of ractangle1("+getb()+","+geth()+") is "+calTriangle();
    }
}
