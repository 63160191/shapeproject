/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class TestSquare {
    public static void main(String[] args){
        Square square1 = new Square(5);
        System.out.println("Area of square1(S = " + square1.getS() + ") is " + square1.calArea());
        square1.setS(25);
        System.out.println("Area of square1(S = " + square1.getS() + ") is " + square1.calArea());
        square1.setS(0);
        System.out.println("Area of square1(S = " + square1.getS() + ") is " + square1.calArea());
        System.out.println(square1);
    }
}
