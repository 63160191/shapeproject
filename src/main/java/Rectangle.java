/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Rectangle {
    private double w;
    private double n;
    public Rectangle(double w,double n){
        this.w = w;
        this.n = n;
    }public double calRectangle(){
        return w*n;
    }
    public void setw(double w){
        if(w<=0){
            System.err.println("Error : Area must more than zero!!!");
            return;
        }
        this.w = w;
    }public void setn(double n){
        if(n<=0){
            System.err.println("Error : Area must more than zero!!!");
            return;
        }
        this.n = n;
    }public void setboth(double w,double n){
        if(n<=0||w<=0){
            System.err.println("Error : Area must more than zero!!!");
            return;
        }
        this.w = w;
        this.n = n;
    }public double getw(){
        return this.w;
    }public double getn(){
        return this.n;
    }
    
    @Override
    public String toString(){
        return "Area of ractangle1("+getw()+","+getn()+") is "+calRectangle();
    }
}
